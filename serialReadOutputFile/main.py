import serial
SERIAL_PORT = "COM8"
ser = serial.Serial(
    port  = SERIAL_PORT,
    baudrate = 250000,
    parity = serial.PARITY_NONE,
    stopbits = serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
)

outFile = open("dataOut.txt", "ab")
while True:
    outFile.write(ser.readline())
