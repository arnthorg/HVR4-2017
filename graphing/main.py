import tkinter as tk
from tkinter import ttk

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib
import matplotlib.animation as animation
from matplotlib import style
import serial
import sys
from matplotlib import pyplot as plt
matplotlib.use("TkAgg")
style.use("ggplot")

LARGE_FONT = ("Verdana", 12)
SERIAL_PORT = "COM8"
f = Figure()
a = f.add_subplot(111)
xList = []
yList = []
ser = serial.Serial(
    port  = SERIAL_PORT,
    baudrate = 250000,
    parity = serial.PARITY_NONE,
    stopbits = serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
)
def main():
    global counter
    counter=0

def animate(i):
    try:
        #x, y = ser.readline().decode('utf-8').split(',')
        try:
            outFile.write("new\n\r")
        except UnboundLocalError:
            outFile = open("dataOut.txt", "ab")
        outFile.write(ser.readline())
        xList.append(int(x))
        yList.append(int(y))
        if int(x) % 1000 == 0:
            a.clear()
            a.plot(xList, yList)
    except ValueError:
        print("packetloss")



class bioscope(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)

        tk.Tk.wm_title(self, "bioscope2000")
        container = ttk.Frame(self)
        container.pack(side = "top", fill = "both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.frames = {}
        for F in (startPage, pageOne):
            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.showFrame(pageOne)

    def showFrame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()


class startPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="Start Page", font = LARGE_FONT)
        label.pack(pady=10, padx=10)
        button1 = ttk.Button(self, text="page 1", command = lambda: controller.showFrame(pageOne))
        button1.pack()


class pageOne(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        label = ttk.Label(self, text="page1", font=LARGE_FONT)
        label.pack(pady=10, padx=10)

        button1 = ttk.Button(self, text="home", command=lambda: controller.showFrame(startPage))
        button1.pack()



        canvas = FigureCanvasTkAgg(f, self)
        canvas.show()
        canvas.get_tk_widget().pack(side=tk.BOTTOM, fill=tk.BOTH, expand=True)

        toolbar = NavigationToolbar2TkAgg(canvas, self)
        toolbar.update()
        canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=True)



app = bioscope()
app.geometry("640x480")
ani = animation.FuncAnimation(f, animate, interval=1)
app.mainloop()

ser.close()